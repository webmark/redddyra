<?php
/**
 * 
 * @package   PKJ - redddyra
 * @author    Petter Kjelkenes <kjelkenes@gmail.com>
 * @license   LGPL
 * @link      http://pkj.no
 * @copyright 2013 Petter Kjelkenes ENK
 *
 * @wordpress-plugin
 * Plugin Name: PKJ - redddyra
 * Plugin URI:  http://pkj.no
 * Description: Logic for theme redddyra
 * Version:     1.0.0
 * Author:      Paal Andr� Sundt, Petter Kjelkenes
 * Author URI:  http://pkj.no
 * Text Domain: redddyra
 * License:     LGPL
 */



add_filter( 'pkj-base-loaded', function () {
	$name = 'redddyra - Webmark';
	$ns = 'ReddDyra';
	$dependencies =array('PkjCore');
	
		
	// -- Bootstrap --
	if (class_exists('PkjCore')) {
		require dirname(__FILE__) . "/lib/$ns.php";
		$pkjCore = PkjCore::getInstance();
		$pkjCore->registerChild(new $ns(
				__DIR__,
				$ns,
				// Dependencies
				$dependencies
		));
	} else {
		add_action( 'admin_notices', function () use ($name) {
			echo sprintf('<div class="error"><p>PKJ - Core plugin is needed for %s</p></div>', $name);
		});
	}
		
} );				
