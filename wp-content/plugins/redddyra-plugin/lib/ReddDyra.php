<?php
class ReddDyra extends PkjCoreChild {
	public function setup() {
		$core = PkjCore::getInstance ();
		
		register_post_type ( 'animal', array (
				'labels' => array (
						'name' => __ ( 'Katt' ),
						'singular_name' => __ ( 'Katt' ),
						'add_new' => 'Legg til katt',
						'add_new_item' => __ ( 'Legg til Katt' ),
						'edit_item' => __ ( 'Rediger Katt' ),
						'new_item' => __ ( 'Ny Katt' ),
						'view_item' => __ ( 'Vis Katt' ),
						'search_items' => __ ( 'S�k i katter' ),
						'not_found' => __ ( 'Ingen katter funnet' ),
						'not_found_in_trash' => __ ( 'Ingen Katter funnet i papirkurven' ),
						'parent' => __ ( 'Forelder katt' ) 
				),
				
				'public' => true,
				'menu_position' => 15,
				'supports' => array (
						'title',
						'thumbnail',
						'editor' 
				),
				'taxonomies' => array (),
				'has_archive' => true,
				'rewrite' => array (
						'slug' => 'katt' 
				) 
		) );
		
		PkjCore::getInstance ()->registerMetaBox ( 'animal_location', 'Lokasjon' );
		PkjCore::getInstance ()->registerPostField ( 'gmaplocation', 'animal', 'location', array (), 'animal_location' );
		
		$labels = array (
				'name' => _x ( 'Tillegsinformasjoner', 'taxonomy general name' ),
				'singular_name' => _x ( 'Tillegsinformasjon', 'taxonomy singular name' ),
				'search_items' => __ ( 'S�k i tillegsinformasjon' ),
				'all_items' => __ ( 'Alle Tillegsinformasjoner' ),
				'parent_item' => __ ( 'Parent Tillegsinformasjon' ),
				'parent_item_colon' => __ ( 'Parent Tillegsinformasjon:' ),
				'edit_item' => __ ( 'Rediger Tillegsinformasjon' ),
				'update_item' => __ ( 'Oppdatert Tillegsinformasjon' ),
				'add_new_item' => __ ( 'Lag ny Tillegsinformasjon' ),
				'new_item_name' => __ ( 'Ny Tillegsinformasjon' ),
				'menu_name' => __ ( 'Tillegsinformasjon' ) 
		);
		
		$args = array (
				'hierarchical' => true,
				'labels' => $labels,
				'show_ui' => true,
				'show_admin_column' => true,
				'query_var' => true,
				'rewrite' => array (
						'slug' => 'dyr-info' 
				) 
		);
		
		register_taxonomy ( 'animal_meta', array (
				'animal' 
		), $args );
		
		$labels = array (
				'name' => _x ( 'Status', 'taxonomy general name' ),
				'singular_name' => _x ( 'Status', 'taxonomy singular name' ),
				'search_items' => __ ( 'S�k i status' ),
				'all_items' => __ ( 'Alle Statuser' ),
				'parent_item' => __ ( 'forelder status' ),
				'parent_item_colon' => __ ( 'forelder status:' ),
				'edit_item' => __ ( 'Rediger status' ),
				'update_item' => __ ( 'Oppdater Status' ),
				'add_new_item' => __ ( 'Lag ny Status' ),
				'new_item_name' => __ ( 'Ny Status' ),
				'menu_name' => __ ( 'Status' ) 
		);
		
		$args = array (
				'hierarchical' => true,
				'labels' => $labels,
				'show_ui' => true,
				'show_admin_column' => true,
				'query_var' => true,
				'rewrite' => array (
						'slug' => 'katt-status' 
				) 
		);
		
		register_taxonomy ( 'animal_status', array (
				'animal' 
		), $args );
		
		add_action ( 'pre_get_posts', function ($query) {
			if( $query->is_tax('animal_status') && get_query_var( 'animal_status' )) {
				$query->set('posts_per_page', 100);
			}
		} );
	}
}
