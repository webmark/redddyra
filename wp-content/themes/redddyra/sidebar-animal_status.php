	<?php if ( is_active_sidebar( 'sidebar-animal_status' ) ) : ?>
		<div class="widget-area widget-area-sidebar widget-area-left widget-area-sidebar-animal_status" role="complementary">
			<?php dynamic_sidebar( 'sidebar-animal_status' ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>