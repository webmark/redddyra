<?php
get_header(); ?>

<div class="col-lg-3 left-sidebar">
	<?php get_sidebar('animal_status')?>

</div>

<div class="col-lg-9">

	<div class="row single-animal">
	<?php 
		$view = "";
		$autopage = PkjCore::getInstance()->child('PkjAutopage');
		
		
		$view = $autopage->autopage(array(
				'query' => &$wp_query,
				'type' => 'full',
				'options' => array(
						'show_author_info' =>  false,
						'show_featured_graphic' => false,
						'show_tags' => false,
						'show_comments' => false,
						'show_attachments' => true
				)
		));
		
		
		echo $view;
	?>
	</div>


</div>
<?php get_footer(); ?>