<?php

add_action('widgets_init', function () {
	register_sidebar( array(
	'name' => __( 'Animal status', 'pkj' ),
	'id' => 'sidebar-animal_status',
	'description' => 'Appears in the animal_status taxonomy',
	'before_widget' => '<div class="widget-body">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>'
	) );
	
});