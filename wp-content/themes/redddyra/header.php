<?php
$pkj_options = get_option('theme_pkj_options');

?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' );?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">
	
	<link href='http://fonts.googleapis.com/css?family=Jura' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' );?>
	<?php wp_head(); ?>
	
</head>
<body  <?php body_class(); ?>>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=582911681746600";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

		<?php 
			$slideshow = apply_filters('pkj_slideshow', array(
				'output' => '',
			));
		?>	

	<header <?php echo !$slideshow['output'] ? 'class="header-no-slideshow"' : ''?>>
			
		<nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo home_url( '/' ); ?>">REDD DYRA</a>
                </div>
                        
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <?php wp_nav_menu( array(
                        'menu'       => 'top_menu',
                        'theme_location' => 'top_menu',
                        'depth'      => 2,
                        'container'  => false,
                        'menu_class' => 'nav navbar-nav',
                        //Process nav menu using our custom nav walker
                        'walker' => new PkjBootstrapNavWalker())
                    ); ?>
                    
                    
                    <?php wp_nav_menu( array(
                        'menu'       => 'right_top_menu',
                        'theme_location' => 'right_top_menu',
                        'depth'      => 2,
                        'container'  => false,
                        'fallback_cb' => false,
                        'menu_class' => 'nav navbar-nav pull-right',
                        //Process nav menu using our custom nav walker
                        'walker' => new PkjBootstrapNavWalker())
                    ); ?>
                    
                <form class="navbar-form navbar-right" action="<?php echo home_url( '/' ); ?>" method="get">
                    <div class="form-group">
                        <input type="text" name="s" id="sitesearch" value="<?php the_search_query(); ?>" class="form-control" placeholder="Søk">	
                    </div>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </form>
        
                </div>
			<!-- /.nav-collapse -->
            </div>
		</nav>
		<!-- /.navbar -->

					
		<?php if ($slideshow['output']):?>
			<div class="header-top">
			    <div class="container">
				<div class="row white-background-content">
					<div class="col-lg-12">
						<?php echo $slideshow['output']?>			
					</div>
				</div>
			    </div>
			</div>
		<?php endif?>
					
					
		
	<?php
	/* wp_nav_menu( array(
						'menu'       => 'banner_links_top',
						'menu_class' => 'banner-top-links',
						'theme_location' => 'banner_links_top',
								'fallback_cb' => false));*/ 
	?>

	</header>

	<div id="wrap">
		<div id="main" class="container clear-top content-main">
			<div class="white-background-content">
				<div class="row">
			
