<?php
get_header(); 

?>
<div class="col-lg-3">
	
	<?php get_sidebar('animal_status')?>

</div>

<div class="col-lg-9">
	
	<div class="row">
		<div class="col-lg-12">
			
			<p class="static-page-description">
				<?php echo term_description(); ?>
			</p>
		</div>
	</div>
	<div class="row">
	
		<div class="col-lg-9">
		<?php 
		$autopage = PkjCore::getInstance()->child('PkjAutopage');
		$view = $autopage->autopage(array(
				'query' => &$wp_query,
				'type' => 'block'
		));
		echo $view;
	
		?>
	</div>
	
		<div class="col-lg-3">
		<?php get_sidebar('right'); ?>
	</div>
	
	</div>

</div>
<?php get_footer(); ?>