					</div>
				</div>
			</div>
		</div>
		
		<footer>

			<div class="footer-widgets">
				<div class="container">
                    <div class="sponsors">
                       <div class="row">
                            <div class="col-md-12">
                                <h1>Våre fantastiske sponsorer:</h1>
                            </div>
                       </div>
                       <div class="row">
                            <div class="col-md-3">
                                <a href="http://www.rising.dyreklinikk.no">
                                    <img src="<?php echo  get_stylesheet_directory_uri() ?>/images/sponsor_logoer/rising_dyreklinikk.png"> 
                                    <div class="image-description">Rising Dyreklinikk</div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="http://gisholt-finne.xl-bygg.no">
                                    <img src="<?php echo  get_stylesheet_directory_uri() ?>/images/sponsor_logoer/XL.png"> 
                                    <div class="image-description">Gisholt & Finne avd. Skien</div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="http://hurtigkopi.no">
                                    <img src="<?php echo  get_stylesheet_directory_uri() ?>/images/sponsor_logoer/hurtig_kopi.png"> 
                                    <div class="image-description">Hurtigkopi AS</div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="http://www.felleskjopet.no">
                                    <img src="<?php echo  get_stylesheet_directory_uri() ?>/images/sponsor_logoer/fk.png"> 
                                    <div class="image-description">Felleskjøpet</div>
                                </a>
                            </div>
                        </div> 
                    </div>
					<div class="row">
						<?php 
							
							$colCount = 4;
							$gridSize = floor(12 / $colCount);
							for ($i = 0; $i < $colCount; $i++) {
								?>
								<div class="col-lg-<?php echo $gridSize?>">
									<?php dynamic_sidebar('footer-wg-' . $i);?>
								</div>
								<?php 
							}
						?>
					
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<div class="row">
						<p><?php echo get_theme_mod('footer_copyright_message')?></p>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_footer()?>
	</body>
</html>
