<?php 
$options = shortcode_atts(array(
	'show_the_title' => true,
	'show_the_excerpt' => true
), $options);?>

<?php if (has_post_thumbnail()):?>
<div class="media-object">
	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(100,700) )?></a>
</div>
<?php else:?>
<div class="media-object">
	<a href="<?php the_permalink(); ?>"><img src="<?php echo get_stylesheet_directory_uri()?>/images/cat_generic.gif" alt="Katt" /></a>
</div>
<?php endif?>

<?php if ($options['show_the_title']):?>
	<h3 class="entry-title">
		<a href="<?php the_permalink(); ?>"
			title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'pkj' ), the_title_attribute( 'echo=0' ) ) ); ?>"
			rel="bookmark"><?php the_title(); ?></a>
	</h3>
<?php endif?>

<?php if ($options['show_the_excerpt']):?>
	<p><?php the_excerpt()?></p>
<?php endif?>