<?php 
$options = shortcode_atts(array(
	'show_the_title' => true,
	'show_author_info' => true,
	'show_featured_graphic' => true,
	'show_tags' => true,
	'show_comments' => false,
	'show_attachments' => false
), $options);

// Must have thumbnail..
$options['show_featured_graphic'] = $options['show_featured_graphic'] &&  has_post_thumbnail();

?>


<div class="col-lg-7 single-animal-left">
	<?php if ($options['show_the_title']):?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	<?php endif?>	
	
	
	<?php if ( $options['show_featured_graphic']):?>
		<div class="media-object">
			<?php the_post_thumbnail()?>
		</div>
	<?php endif?>	
	
	<div class="the_content <?php if ($options['show_featured_graphic']) {echo "the_content_has_graphic";}?>">
		<?php the_content( ); ?>
	</div>
	
	
	
	<h3>Kommentarer</h3>
	<div class="fb-comments" data-href="<?php the_permalink()?>" data-colorscheme="light" data-numposts="5" data-width="482"></div>
	
	
</div>
<div class="col-lg-5 single-animal-right">
	
	
	
	<div class="single-animal-right-thumbnail">
		<div class="media-object">
			<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" class="fancybox">
			<?php the_post_thumbnail('large')?>
			</a>
		</div>
	</div>
	
	
	
		<?php 
			$metas = wp_get_post_terms($post->ID, 'animal_meta');
		?>
		<?php if (count($metas) > 0):?>
			<div class="widget-area widget-area-sidebar" style="margin-top: 10px;">
			<div class="widget-body">
				<h3 class="widget-title">Informasjon</h3>
				
				<div class="animal_meta_section textwidget">
					<ul>
						<?php foreach($metas as $meta):?>
						<li><span class="glyphicon-<?php echo $meta->slug?>"></span> <?php echo $meta->name?></li>
						<?php endforeach?>
					</ul>
				</div>
			</div>
			</div>
		<?php endif?>
	
	
	<?php 
	$ownerTerms = wp_get_post_terms($post->ID, 'animal_status');
	$showMap = false;
	foreach($ownerTerms as $t) {
		if ($t->slug == 'savnet' || $t->slug == 'funnet') {
			$showMap = true;
			break;
		}
	}
	$ownerTerms = array_values($ownerTerms);
	$selectedTerm = isset($ownerTerms[0]) ? $ownerTerms[0]: null;
	?>
	
	<?php if ($showMap):?>
	<div class="single-animal-right-map">
		<?php if ($selectedTerm):?>
			<?php 
			$heading = 'Lokasjon';
			switch($selectedTerm->slug){
				case "savnet":
					$heading = 'Sist sett';
					break;
				case "funnet":
					$heading = 'Oppholdssted';
					break;
			}
			?>
			<h4 class="single-animal-right-map-header"><?php echo $heading?></h4>
		<?php endif?>
		<?php echo PkjCore::getInstance()->getData('location')?>
	
	</div>
	<?php endif?>
	
	
</div>
	